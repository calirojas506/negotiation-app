import Vue from 'vue'
import TabsComponent from '@/components/TabsComponent'

describe('TabsComponent.vue', () => {
  const theConstructor = Vue.extend(TabsComponent)
  const theComponent = new theConstructor().$mount()

	it('Show results only when both forms are submitted', () => {
    theComponent.employer.submitted = true
    theComponent.employee.submitted = true

    expect(theComponent.showResult).toBe(true)
  })

	it('Is success when minimum salary is equal or less than offer', () => {
    theComponent.employer.maxOffer = 500
    theComponent.employee.minimumSalary = 500

    expect(theComponent.isSuccess).toBe(true)
  })

})
